#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "des.h"

#define DEF_KEY_FILE "standard_key"

typedef struct {
  bool is_valid;
  char message[1000];
  char infile[100];
  char outfile[100];
  bool custom_key;
  bool has_infile;
  bool has_outfile;
  char keyfile[100];
  bool is_interactive;
  bool decrypt_mode;
} Cli;

// infile rot encryption
void encrypt_by_rot(char *plain_text, char *cipher_text, int rot,
                    bool decrypt_mode);

char *get_welcome_msg() {
  return "Welcome To Cipher!!\nUsage: ./cipher [input.txt] [output.txt] "
         "[--interactive|-i:optional] [--decrypt|-d:optional]";
}

char *get_help_msg() {
  return ":: Usage: ./cipher [input.txt] [output.txt] [--key|-k:optional] key";
}
char *get_interactive_mode_welcome_msg() {
  return "Welcome to interactive Mode!\nType in any string to encrypt or 'q' "
         "to quit.";
}

char *get_decrypt_mode_msg() { return "\n:: Running on Decryption Mode!\n"; }

void filter_args_on_priority(char *arg, Cli *cli) {
  if (strcmp(arg, "--interactive") == 0 || strcmp(arg, "-i") == 0) {
    cli->is_interactive = true;
    cli->is_valid = true;
    strcat(cli->message, get_interactive_mode_welcome_msg());
  } else if (strcmp(arg, "--decrypt") == 0 || strcmp(arg, "-d") == 0) {
    cli->decrypt_mode = true;
    strcat(cli->message, get_decrypt_mode_msg());
  } else if (cli->has_infile == false) {
    strcpy(cli->infile, arg);
    cli->has_infile = true;
  } else if (cli->has_outfile == false) {
    strcpy(cli->outfile, arg);
    cli->has_outfile = true;
    cli->is_valid = true;
  }
}

Cli parse_cli_args(int argc, char *argv[]) {
  Cli cli;

  // Assure valid generally and set it to true in some conditions
  cli.is_valid = false;
  cli.is_interactive = false;
  cli.decrypt_mode = false;
  cli.has_infile = false;
  cli.has_outfile = false;
  cli.custom_key = false;

  // No of arguments provided (argc is always 1 since first one is the name of
  // our program itself)
  int arguments = argc - 1;

  // Go through each args and fill out cli struct
  for (int i = 1; i <= arguments; i++) {
    filter_args_on_priority(argv[i], &cli);
  }

  // Help msg when wrong arguments
  if (cli.is_valid == false) {
    strcpy(cli.message, get_help_msg());
  }

  // Welcome screen when no arguments provided
  if (arguments == 0) {
    strcpy(cli.message, get_welcome_msg());
  }

  return cli;
}

void load_interactive_mode(bool decrypt_mode) {
  while (1) {
    char buf[100];
    char en_buf[100];
    printf(">> ");
    scanf(" %s", buf);
    if (strcmp("q", buf) == 0) {
      break;
    }
    encrypt_by_rot(buf, en_buf, 13, decrypt_mode);
    printf("%s\n\n", en_buf);
  }
}

int read_file(char *filename, char *content) {
  FILE *file = fopen(filename, "r");
  if (file == NULL) {
    return 0;
  }
  int i = 0;
  while (1) {
    int read = fread(&content[i], sizeof(char), 1, file);
    if (read != 1)
      break;
    i++;
  }
  return 1;
}

int write_file(char *filename, char *content) {
  FILE *file = fopen(filename, "w");
  if (file == NULL) {
    return 0;
  }
  int wrote = fwrite(content, strlen(content) * sizeof(char), 1, file);
  /* printf("fwrite returned: %d\n", wrote); */
  return wrote;
}

void encrypt_by_rot(char *plain_text, char *cipher_text, int rot,
                    bool decrypt_mode) {
  int i;

  for (i = 0; plain_text[i] != '\0'; i++) {
    const int UPPER_MIN = 65, UPPER_MAX = 90, LOWER_MIN = 97, LOWER_MAX = 122;
    int is_lower_alpha =
        plain_text[i] >= LOWER_MIN && plain_text[i] <= LOWER_MAX;
    int is_upper_alpha =
        plain_text[i] >= UPPER_MIN && plain_text[i] <= UPPER_MAX;
    int offset_value = is_lower_alpha ? LOWER_MIN : UPPER_MIN;

    if (is_lower_alpha || is_upper_alpha) {
      char offseted_alpha = plain_text[i] - offset_value;
      char encrypted_alpha = (offseted_alpha + rot) % 26;
      if (decrypt_mode == true) {
        encrypted_alpha = (offseted_alpha + rot) % 26;
      }
      cipher_text[i] = offset_value + encrypted_alpha;
    } else {
      cipher_text[i] = plain_text[i];
    }
  }
  cipher_text[i] = '\0';
}

int ensure_file_op(FILE *infile, FILE *outfile, FILE *keyfile) {
  if (infile == NULL) {
    printf("Could not read file \n");
    return 0;
  }
  if (outfile == NULL) {
    fclose(infile);
    printf("Could not write file\n");
    return 0;
  }
  if (keyfile == NULL) {
    fclose(infile);
    fclose(outfile);
    printf("Could not read standard key file\n");
    return 0;
  }
  return 1;
}

int ensure_key_file(char *keyfile) {
  if (access(keyfile, F_OK) != 0) {
    FILE *key_f = fopen(keyfile, "w");
    if (key_f == NULL) {
      printf("No key file '%s' present, failed creating new one!\n", keyfile);
      return 0;
    }
    fprintf(key_f, "%lu\n", generate_random_key());
    fclose(key_f);
  }
  return 1;
}

int encrypt_file(char *infile, char *outfile, bool decrypt_mode) {
  FILE *infile_p = fopen(infile, "r"), *outfile_p = fopen(outfile, "w"), *key_p;
  key_p = fopen(DEF_KEY_FILE, "r");
  if (!ensure_file_op(infile_p, outfile_p, key_p)) {
    return 0;
  }
  uint64_t key = generate_random_key();
  printf("Key is %lu\n", key);
  fscanf(key_p, "%lu\n", &key);
  printf("Key is %lu\n", key);

  /* printf("%d", decrypt_mode); */
  des_encrypt_file(infile_p, outfile_p, key, decrypt_mode);
  /* printf("Wrote to file content:\n %s\n", cipher_text); */
  if (decrypt_mode == false) {
    printf("Encrypted file %s -> %s\n", infile, outfile);
  } else {
    printf("Decrypted file %s -> %s\n", infile, outfile);
  }
  return 1;
}

int main(int argc, char *argv[]) {
  /* printf(":: Received %d arguments. %s | %s | %s\n", argc, argv[1], argv[2],
   * argv[3]); */
  /* printf(":: %s is reading input file '%s'....\n\n\n", argv[0], argv[1]); */
  Cli cli = parse_cli_args(argc, argv);
  if (cli.is_valid == false) {
    puts(cli.message);
  } else if (cli.is_interactive == true) {
    puts(cli.message);
    load_interactive_mode(cli.decrypt_mode);
  } else if (cli.is_valid == true && cli.has_infile == true &&
             cli.has_outfile == true) {
    char *infile = cli.infile;
    char *outfile = cli.outfile;
    puts(cli.message);
    /* printf("Got infile and outfile %s -> %s\n", infile, outfile); */
    if (!ensure_key_file(DEF_KEY_FILE)) {
      return -1;
    }
    encrypt_file(infile, outfile, cli.decrypt_mode);
  }
  return 0;
}
