/* Copyright (C) 2021 by boink */
/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include "des.h"
/* all arrays are defined as global constant variables in sboxes.c */
#include "sboxes.c"
/* for random no generation */
#ifdef __unix__
#include <unistd.h>
#else
#include <time.h>
#include <sys/random.h>
#endif

struct data{
  uint64_t plaintext;
  uint32_t right;
  uint32_t left;
};

#define ARR_LEN(x) (sizeof(x)/sizeof(x[0]))
uint32_t des_func(uint32_t data, uint64_t round_key);
uint64_t apply_s_box_arr(uint32_t data,int *arr);
uint32_t s_box(uint64_t data);
uint8_t apply_s_box(uint8_t data, int box_no);
void de_initialize_data(struct data*d);
void gen_partial_key(uint64_t full_key,uint64_t *arr, int do_encryption);
void swapper(struct data *d);
uint64_t permutate(uint64_t data,const int *array, unsigned int len);
void initialize_data(struct data *d,uint64_t text);
void feistel(struct data *d, uint64_t round_key);
uint64_t substitute_nth_bit_from_arr(uint32_t data,int *arr,unsigned int);

uint64_t des_encrypt(uint64_t plaintext, uint64_t full_key, int do_encryption){
  struct data result;
  initialize_data(&result,plaintext);
  uint64_t partial_key_arr[16];
  gen_partial_key(full_key,partial_key_arr,do_encryption);
  
  for(int i=0;i<16;i++){
    /* printf("plaintext before feistel: %lx\n",result.plaintext); */
    feistel(&result,*(partial_key_arr+i));
    /* printf("plaintext after feistel: %lx\n",result.plaintext); */
  }
    de_initialize_data(&result);
  return result.plaintext;
}

int des_encrypt_file(FILE *in, FILE *out, uint64_t key, int enc_or_dec){
  if(!in || !out){
    return -1;
  }
  uint64_t *buf,*res;
  /* printf("Inc or dec is %d", enc_or_dec); */
  int n=10,buf_size=sizeof (*buf) * n;
  buf = malloc(buf_size);
  res = malloc(buf_size);
  if(!buf || !res){
    free(buf); free(res);
    return -2;
  }
  int len=0;
  while((len=fread(buf,sizeof(*buf),n,in))>0){
    for(int i=0;i<len;i++){
      *(res+i)=des_encrypt(*(buf+i),key,enc_or_dec);
    }
    fwrite(res,sizeof(*res),len,out);
  }
  free(buf);free(res);
  return 0;
}

uint64_t generate_random_key(){
  uint64_t key;
#ifdef __unix__
  if(getentropy(&key,sizeof(key))){
    /* perror("can't generate key"); */
    /* exit(1); */
    return 0;
  }
#else
  srand(time(0));
  key = rand() << 48 |
    (rand() << 48) >> 16 |
    (rand() << 48) >> 32 |
    (rand() << 48) >> 48;
#endif
  return key;
}

void gen_partial_key(uint64_t full_key,uint64_t *arr,int do_encryption){
  /* printf("The key is: %lx\n", full_key); */
  if(!arr)
    return;
  full_key = permutate(full_key,remove_parity_arr,ARR_LEN(remove_parity_arr));
  /* full key is 56 bits. 56/2 = 28 */
  struct data d = {full_key, full_key << 28, full_key >> 28};
  int shift,start=0,end=16,incr_by=1;
  if(!do_encryption){
    start = 15;end = -1;incr_by=-1;
  }
  int count =0;
  for(int i=start;i!=end;i+=incr_by){
    if(count==0||count==1||count==8||count==15)
      shift = 1;
    else
      shift = 2;
    d.left = ( d.left >> shift ) | (d.left << (28-shift));
    d.right = ( d.right >> shift ) | (d.right << (28-shift));
    d.plaintext = d.left << 28 | d.right;
    *(arr+i) = permutate(d.plaintext,partial_key_compression_arr,ARR_LEN(partial_key_compression_arr));
    count++;
  }
}

void initialize_data(struct data *d,uint64_t text){
  /* puts("Initializing..."); */
  /* printf("initial plaintext: %lx\n",text); */
  text = permutate(text,initial_permutation,64);
  /* printf("permuted plaintext: %lx\n",text); */
  d->plaintext = text;
  d->right = (text << 32) >> 32;
  d->left = text >> 32;
  /* printf("Plaintext: %lx\n",d->plaintext); */
  /* printf("left: %x\n",d->left); */
  /* printf("right: %x\n",d->right); */
}

void swapper(struct data *d){
  uint32_t tmp;
  tmp = d->right;
  d->right = d->left;
  d->left = tmp;
  
  d->plaintext = (uint64_t) d->left;
  d->plaintext <<= 32;
  d->plaintext = d->plaintext | ((uint64_t)d->right);
}
void mixer(struct data *d, uint64_t round_key){
  uint32_t tmp = des_func(d->right,round_key);
  d->left ^= tmp;
}

uint32_t des_func(uint32_t data, uint64_t round_key){
  uint64_t expanded = permutate(data,expansion_permutation,48);
  expanded ^= round_key;
  /* printf("The expanded form of data (%x) is: %lx\n",data,expanded); */
  uint32_t s_ret = s_box(expanded);
  /* the permutation box is simple substitution */
  uint32_t p_ret = permutate(s_ret,s_box_permutation,32);
  /* printf("After s-box: %x\n",s_ret); */
  /* printf("After permutation: %x\n",p_ret); */
  return p_ret;
}

uint32_t s_box(uint64_t data){
  uint32_t result=0,tmp=0;
  uint8_t chunks[8] = {0};
  for(int i=0;i<8;i++){
    chunks[i]  = data & 0b111111;
    data >>= 6;
    tmp = apply_s_box(chunks[i],i);
    result = (result << 4) | tmp;
  }
  return result;
}

uint8_t apply_s_box(uint8_t data, int box_no){
  /* take a 6 bit value and return 4 bit value  */
  uint8_t result = 0;
  uint8_t row=0,coloumn=0;
  
  row = data & 1;
  row <<= 1;
  row = row | ((data & 0b00100000) >> 5) ;

  coloumn = data << 1;
  coloumn = coloumn >> 2;
  
  result = s_boxes_arr[box_no][row][coloumn];
  return result;  
}

void feistel(struct data *d, uint64_t round_key){
  /* only 48 bits is needed. */
  round_key <<=16;
  round_key >>= 16;
  mixer(d,round_key);
  swapper(d);
}

void de_initialize_data(struct data*d){
  swapper(d);
  d->plaintext = permutate(d->plaintext,final_permutation,64);
}

uint64_t permutate(uint64_t data,const int *array, unsigned int len){
  uint64_t result = 0,tmp=0,tmp2=0;
  /* take nth bit of data as specified by array and put that into current bit on result */
  int offset = 0;
  for(unsigned int i=0;i<len;i++){
    offset = array[i];
    tmp = 1 << offset;
    tmp2 = tmp & data;
    result = result | tmp2;
    tmp2 = tmp = 0;
  }
  return result;
}
