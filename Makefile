# This is the Makefile for our program
# Typing 'make' or 'make cipher' will create the executable file
# Then execute the executable using ./cipher
# Typing 'make clean/clear (win/linux)' will remove all the executable files and object files

CC = gcc
CFLAGS = -ggdb3 -O0 -Wall -Werror -Wextra -Wshadow -std=c11

# compiler flags:
#  -g       adds debugging information to the executable file
#  -Wall    turns on most, but not all, compiler warnings
#  -Werror  makes all warnings into errors
#  -Wextra  enables some extra warning flags that are not enabled by -Wall
#  -Wshadow warns when a local variable or type declaration shadows another variable
#  -std=c11 sets the language standard to C11

TARGET = cipher

all: $(TARGET)

$(TARGET): cipher.o des.o
	$(CC) $(CFLAGS) -o $(TARGET) cipher.o des.o

cipher.o: des.o cipher.c des.h
	$(CC) $(CFLAGS) -c cipher.c

des.o: des.c
	$(CC) -c des.c

.PHONY: clean
clean:
	del -f *.o *.exe *~ 

.PHONY: clear
clear:
	rm des.o cipher.o cipher
